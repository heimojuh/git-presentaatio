# GIT

## Hajautettu versionhallinta
- Keskitetty (subversion) vs. hajautettu
- Kaikki repositoryt ovat saman vertaisia

## Git Lokaalisti
- Työhakemisto
- Stage / index
- Repository
- Kommitoiminen
- Haarat. (git branch)

### Merge
- Pointteri, HEAD
- Fast forward ja no-Fast Forward merge
- Fast forward -> pointterin siirto, koska historia on lineaarinen.
- Muutoin merge tekee merge commitin, koska historia ei ole ollut lineaarinen.

### Rebase
- Koskee historiaan
- ts. älä tee haaroille jotka työnnät sellaisenaan muiden käyttöön.

### Rebase interactive
- Kuinka pidetään historiatieto siistinä.

## Git remote
- Toimiminen etä-repositoryjen kanssa
- "Origin"

### Push

### Pull
- Kuinka päivitän oman lokaalin repositoryni, ja feature-haarani.

#GIT Workflowt

## Git Flow
- Kaksi pysyvää haaraa: Master ja Development.
- Master koko ajan ns. julkaisukunnossa
- Feature / kehityshaarat otetaan Developmentista ja mergetään Developmentiin.
- Feature / Kehityshaarat elävät pääosin pelkästään kehittäjän yksityisessä repositoriossa.

## Github Flow
- Yksi pysyvä haara: Master.
- Master koko ajan julkaistavissa.
- Topic / feature otetaan masterista
- Topic / feature haarat työnnetään samoin tein originiin, ja päivitetään originissa.

## Dirtymaster Flow
- Sama kuin Github flow, mutta kahdellta pysyvällä haaralla:
- Master ja Dirtymaster.
- Topic haarat otetaan dirtymasterista, ja mergetään dirtymasteriin.
- Dirtymaster mergetään ajoittain masteriin.
- Master kokoajan julkaistavissa.

#Git Subtree
- Silloin kun projekti koostuu useammasta repositorystä, esim kun halutaan forkata 3 osapuolen kirjasto.

- Pitää ns. päärepositoryn ehjänä, eikä vaaadi kloonaajalta oikeuksia lapsi-repositoryihin.

- Tarvitsee uudehkon gitin, ja lisäkomentoja.
